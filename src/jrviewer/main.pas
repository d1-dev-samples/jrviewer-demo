unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IdTCPClient, IdGlobal, JrbustcpClient,
  ExtCtrls, ComCtrls, Grids, BaseGrid, AdvGrid, Clipbrd, Menus, Math;

type
  TForm1 = class(TForm)
    timer: TTimer;
    edPort: TEdit;
    cmbLocalFilter: TComboBox;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    edHost: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    edPeriod: TEdit;
    Label8: TLabel;
    pnlConnect: TPanel;
    edClientDescr: TEdit;
    edRemoteFilter: TEdit;
    grid: TAdvStringGrid;
    PopupMenu1: TPopupMenu;
    mnCopyToClipboard: TMenuItem;
    N1: TMenuItem;
    mnSelectAll: TMenuItem;
    Memo1: TMemo;
    pnlTotal: TPanel;
    Label3: TLabel;
    pnlListSize: TPanel;
    lbListSize: TLabel;
    cbExcludeExternal: TCheckBox;
    cbIncludeHidden: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure pnlConnectClick(Sender: TObject);
    procedure timerTimer(Sender: TObject);
    procedure cmbLocalFilterKeyPress(Sender: TObject; var Key: Char);
    procedure gridResize(Sender: TObject);
    procedure gridEditCellDone(Sender: TObject; ACol, ARow: Integer);
    procedure mnCopyToClipboardClick(Sender: TObject);
    procedure mnSelectAllClick(Sender: TObject);
    procedure edPeriodChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure gridEditChange(Sender: TObject; ACol, ARow: Integer;
      Value: String);
    procedure gridCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure gridGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    connected: boolean;
    clnt: TJrbustcpClient;
    iniFileName: string;
    listsize: integer;
    runOnStartup: boolean;
    editMode: integer;
    editRow: integer;

    procedure UpdateConnectPanel;
    procedure UpdateList;
    procedure UpdateValues;
    procedure ApplyLocalFilter;
    procedure LoadParamStr;
    procedure LoadIni;
    procedure SaveIni;
  public
  end;


var
  Form1: TForm1;

implementation
{$R *.dfm}

uses
  inifiles,
  Tags,
  HyperStr;




procedure TForm1.FormCreate(Sender: TObject);
begin
  runOnStartup := false;

  iniFileName := ChangeFileExt(Application.ExeName, '.ini');

  LoadIni;
  LoadParamStr;

  clnt := TJrbustcpClient.Create;
  connected := false;
  UpdateConnectPanel;

//  grid.Alignments[1,0] := taCenter;
//  grid.Alignments[2,0] := taCenter;

  grid.doublebuffered := true;
  grid.Cells[0,0] := 'Name';
  grid.Cells[1,0] := 'Type';
  grid.Cells[2,0] := 'Value';
  grid.ColWidths[0] := 200;
  gridResize(nil);

  listsize:=0;
  editMode := 0;
  editRow := -1;

end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  clnt.Free;
  SaveIni;
end;

procedure TForm1.pnlConnectClick(Sender: TObject);
begin
  connected := not connected;
  pnlConnect.BevelOuter := bvLowered;
  Application.ProcessMessages;

  if connected then begin
    clnt.host := edHost.Text;
    clnt.port := StrToIntDef(edPort.Text, 0);
    clnt.filter := edRemoteFilter.Text;
    clnt.descr := edClientDescr.Text;
    clnt.period := StrToIntDef(edPeriod.Text, 1000);
    timer.Interval := clnt.period;

    clnt.setFlag(INITPRM_EXCLUDE_EXTERNAL, cbExcludeExternal.Checked);
    clnt.setFlag(INITPRM_INCLUDE_HIDDEN, cbIncludeHidden.Checked);

    clnt.Connect;
    UpdateList;
  end else begin
    clnt.Disconnect;
  end;

  pnlConnect.BevelOuter := bvRaised;
  UpdateConnectPanel;
end;


procedure TForm1.UpdateConnectPanel;
const
  clOn = clWhite;
  clOff = clBtnFace;
var
  cl1, cl2: TColor;
  i: integer;
begin
  if connected then begin
    pnlConnect.Color := $004242FF;
    pnlConnect.Font.Color := clWhite;
    pnlConnect.Caption := 'Disconnect';
    cl1 := clOff;
    cl2 := clOn;
  end else begin
    pnlConnect.Color := $0080FF80;
    pnlConnect.Font.Color := clBlack;
    pnlConnect.Caption := 'Connect';
    cl1 := clOn;
    cl2 := clOff;
  end;

  edClientDescr.Color     := cl1;
  edHost.Color            := cl1;
  edPort.Color            := cl1;
  edRemoteFilter.Color    := cl1;
  edRemoteFilter.Color    := cl1;

  grid.BackGround.Color   := cl2;
  grid.BackGround.ColorTo := cl2;
  cmbLocalFilter.Color    := cl2;
end;


procedure TForm1.timerTimer(Sender: TObject);
begin
  if connected then begin
    if not clnt.Connected then begin
      pnlConnectClick(nil);
      Exit;
    end;

    if clnt.NeedReinitList then begin
      clnt.Disconnect;
      clnt.Connect;
      UpdateList;
    end;

    UpdateValues;
  end;

end;



function CheckFilter(src, flt: string): boolean;
var
  i,j,k: integer;
  stmp, sflt: string;
  flag: boolean;
begin
  flag := false;
  stmp := trim(flt);
  repeat
    k := pos(';', stmp);
    if k > 0 then begin
      sflt := copy(stmp, 1, k-1);
      delete(stmp, 1, k);
    end else begin
      sflt := stmp;
      stmp := '';
    end;

    if length(sflt) > 0 then begin
      i := 1;
      j := ScanW(src, sflt, i);
      if i=1 then
        if j=length(src) then flag := true
        else flag := sflt[length(sflt)] = '*'
      else
        flag := false;
    end else
      flag := true;

    if flag then break;
  until stmp = '';

  result := flag;
end;


procedure TForm1.UpdateList;
var
  i,r,tblsize: Integer;
  tag: TTag;
  itags: array of integer;
  filter: string;
begin
  filter := Trim(cmbLocalFilter.Text);
  if filter = '' then
    filter := '*';

  tblsize := clnt.TagTable.getSize;
  SetLength(itags, tblsize);
  listsize := 0;
  for i:=0 to tblsize-1 do begin
    tag := clnt.TagTable.get(i);
    if CheckFilter(tag.getName, filter) then begin
      itags[ listsize ] := i;
      inc(listsize);
    end;
  end;
  SetLength(itags, listsize);

  pnlTotal.Caption := IntToStr(clnt.TagTable.getSize);
  pnlListSize.Caption := IntToStr(listsize);

  if listsize = 0 then begin
    grid.RowCount := 2;
    grid.Cells[0,1] := '';
    grid.Cells[1,1] := '';
    grid.Cells[2,1] := '';
  end else
    grid.RowCount := listsize+1;

  for i:=0 to listsize-1 do begin
    tag := clnt.TagTable.get(itags[i]);
    r := i+1;

    if grid.Cells[0,r] <> tag.getName then
      grid.Cells[0,r] := tag.getName;

    // todo: descr

    if grid.Cells[1,r] <> tag.getTypeName then
      grid.Cells[1,r] := tag.getTypeName;

    if grid.Cells[2,r] <> tag.getString then
      grid.Cells[2,r] := tag.getString;

    grid.Objects[0,r] := Pointer(itags[i]);

//    grid.Alignments[1,r] := taCenter;
//    grid.Alignments[2,r] := taCenter;
  end;

end;



procedure TForm1.UpdateValues;
var
  i,n,index: Integer;
  tag: TTag;
  cl: TColor;
begin
  n := min(grid.RowCount, grid.TopRow + grid.VisibleRowCount);
  for i:=grid.TopRow to n do begin
    index := Integer(grid.Objects[0,i]);
    if index >= clnt.TagTable.getSize then begin
      //ShowMessage(IntToStr(index));
      Continue;
    end;
    tag := clnt.TagTable.get( index );

    if (editMode > 0) and (i = editRow) then Continue;

    if grid.Cells[2,i] <> tag.getString then begin
      grid.Cells[2,i] := tag.getString;
    end;
  end;

  if editMode = 2 then begin
    editRow := -1;
    editMode := 0;
  end;
end;


procedure TForm1.ApplyLocalFilter;
begin
  if (cmbLocalFilter.Items.count = 0) or (cmbLocalFilter.Items[0] <> cmbLocalFilter.Text) then
    cmbLocalFilter.Items.Insert(0, cmbLocalFilter.Text );

  UpdateList;
end;

procedure TForm1.cmbLocalFilterKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then begin
    ApplyLocalFilter;
    Key := #0;
  end;
end;


procedure TForm1.LoadParamStr;
var
  i,k: Integer;
  prm,value: string;
begin
    for i:=1 to ParamCount do begin
      prm := trim(ParamStr(i));
      k := Pos('=', prm);
      if k = -1 then Continue;
      value := Copy(prm, k+1, Length(prm)-k);
      Delete(prm, k, Length(prm)-k+1);
      prm := UpperCase(prm);

      if prm = '-FILTER' then edRemoteFilter.Text := value;
      if prm = '-DESCR' then edClientDescr.Text := value;
      if prm = '-HOST' then edHost.Text := value;
      if prm = '-PORT' then edPort.Text := value;
      if prm = '-PERIOD' then edPeriod.Text := value;
      if prm = '-LOCALFILTER' then cmbLocalFilter.Text := value;
      if prm = '-START' then runOnStartup := true;
    end;
end;



procedure TForm1.LoadIni;
var
  gr: string;
  flags: integer;
begin
  with TIniFile.Create(iniFileName) do try
    gr := 'JrbustcpClient';;
    edRemoteFilter.Text := ReadString(gr, 'filter', '');
    edClientDescr.Text  := ReadString(gr, 'descr', 'viewer');
    edHost.Text         := ReadString(gr, 'host', 'localhost');
    edPort.Text         := ReadString(gr, 'port', '30000');
    edPeriod.Text       := ReadString(gr, 'period', '500');
    cmbLocalFilter.Text := ReadString(gr, 'localfilter', '*');
    cbExcludeExternal.Checked := ReadBool(gr, 'excludeExternal', false);
    cbIncludeHidden.Checked := ReadBool(gr, 'includeHidden', false);

    gr := 'MainForm';
    Form1.Left   := ReadInteger(gr, 'Left', Form1.Left);
    Form1.Top    := ReadInteger(gr, 'Top', Form1.Top);
    Form1.Width  := ReadInteger(gr, 'Width', Form1.Width);
    Form1.Height := ReadInteger(gr, 'Height', Form1.Height);

  finally
    Free;
  end;
end;

procedure TForm1.SaveIni;
var
  gr: string;
begin
  with TIniFile.Create(iniFileName) do try
    gr := 'JrbustcpClient';;
    writeString(gr, 'filter', edRemoteFilter.Text);
    writeString(gr, 'descr', edClientDescr.Text);
    writeString(gr, 'host', edHost.Text);
    writeString(gr, 'port', edPort.Text);
    writeString(gr, 'period', edPeriod.Text);
    writeString(gr, 'localfilter', cmbLocalFilter.Text);
    WriteBool(gr, 'excludeExternal', cbExcludeExternal.Checked);
    WriteBool(gr, 'includeHidden', cbIncludeHidden.Checked);

    gr := 'MainForm';
    writeInteger(gr, 'Left', Form1.Left);
    writeInteger(gr, 'Top', Form1.Top);
    writeInteger(gr, 'Width', Form1.Width);
    writeInteger(gr, 'Height', Form1.Height);
  finally
    Free;
  end;
end;

procedure TForm1.gridResize(Sender: TObject);
begin
  grid.ColWidths[1] := 80;
  grid.ColWidths[2] := 180;
  grid.AutoFitColumns();
end;


//procedure TForm1.gridKeyPress(Sender: TObject; var Key: Char);
//begin
//  if Key = #13 then begin
//    gridDblClickCell(nil,0,0);
//  end;
//end;


procedure TForm1.mnCopyToClipboardClick(Sender: TObject);
var
  Clip: TClipboard;
begin
    Clip := TClipboard.Create;
    Clip.AsText := grid.SelectedText;
    Clip.Free;
end;

procedure TForm1.mnSelectAllClick(Sender: TObject);
begin
  grid.SelectRange(0,grid.ColCount-1, 1,grid.RowCount-1);
end;

procedure TForm1.edPeriodChange(Sender: TObject);
var
  period: Integer;
begin
  if not connected then Exit;
  
  period := StrToIntDef(edPeriod.Text, 1000);
  if period < 10 then begin
    period := 10;
    edPeriod.Text := '10';
  end;
  clnt.period := period;
  timer.Interval := period;
end;

procedure TForm1.FormActivate(Sender: TObject);
begin
  if runOnStartup then
    pnlConnectClick(nil);
end;

procedure TForm1.gridEditCellDone(Sender: TObject; ACol, ARow: Integer);
var
  tag: TTag;
begin
  if listsize = 0 then Exit;
  tag := clnt.TagTable.get( Integer(grid.Objects[0, ARow]) );
  tag.setString( grid.Cells[acol, arow] );
  editMode := 2;
end;


procedure TForm1.gridEditChange(Sender: TObject; ACol, ARow: Integer;
  Value: String);
begin
  editRow := arow;
  editMode := 1;
end;

procedure TForm1.gridCanEditCell(Sender: TObject; ARow, ACol: Integer;
  var CanEdit: Boolean);
begin
  CanEdit := ACol = 2;
end;


procedure TForm1.gridGetCellColor(Sender: TObject; ARow, ACol: Integer;
  AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
var
  index: Integer;
  tag: TTag;
  cl: TColor;
begin

  cl := clBlack;
  if ARow > 0 then begin
    index := Integer(grid.Objects[0,ARow]);
    if index < clnt.TagTable.getSize then begin
      tag := clnt.TagTable.get( index );
      if tag.getStatus <> tsGOOD then
        cl := clRed;
    end;
  end;
  AFont.Color := cl;

end;

end.
