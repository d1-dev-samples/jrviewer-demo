unit Mutex;

interface

uses
  SysUtils, Windows;
  
type

{ WIN32 Helper Class }

{ This class encapsulates the concept of a Win32 mutex.  See "CreateMutex"
  in the Win32 reference for more information }

  TMutex = class(TObject)
  protected
    FHandle: THandle;
  public
    constructor Create(const Name: string);
    function Get(TimeOut: Integer): Boolean;
    function Release: Boolean;
    destructor Destroy; override;
    property Handle: THandle read FHandle;
  end;


implementation

destructor TMutex.Destroy;
begin
  if FHandle <> 0 then
    CloseHandle(FHandle);
end;

constructor TMutex.Create(const Name: string);
begin
  FHandle := CreateMutex(nil, False, PChar(Name));
  if FHandle = 0 then abort;
end;

function TMutex.Get(TimeOut: Integer): Boolean;
begin
  Result := WaitForSingleObject(FHandle, TimeOut) = WAIT_OBJECT_0;
end;

function TMutex.Release: Boolean;
begin
  Result := ReleaseMutex(FHandle);
end;


end.
