unit Tags;

interface
uses
  Mutex;

const
  WRITE_MUTEX_TIMEOUT = 1000;
  TAG_FLAG_EXTERNAL = 1;

type
  TTagType = (ttBOOL=1, ttINT=2, ttLONG=3, ttDOUBLE=4, ttSTRING=5);
  TTagStatus = (tsUNINIT, tsGOOD, tsBAD, tsDELETED);

  TTag = class(TObject)
  protected
    writeMutex: TMutex;
    name: string;
    status: TTagStatus;
    flags: Integer;
  public
    constructor Create(name: string; flags: Integer = 0; writeMutex: TMutex = nil);virtual;

    function getName: string;
    function getType: TTagType; virtual; abstract;
    function getTypeName: string;
    function getStatus: TTagStatus;
    procedure setStatus(status: TTagStatus);

    function getFlags: Integer;
    procedure setFlags(flags: Integer);

    function getBool: boolean; virtual; abstract;
    function getInt: integer; virtual; abstract;
    function getLong: int64; virtual; abstract;
    function getDouble: double; virtual; abstract;
    function getString: string; virtual; abstract;

    procedure setBool(value: boolean); virtual; abstract;
    procedure setInt(value: integer); virtual; abstract;
    procedure setLong(value: int64); virtual; abstract;
    procedure setDouble(value: double); virtual; abstract;
    procedure setString(value: string); virtual; abstract;

    function equalsValue(tag: TTag): Boolean; virtual; abstract;
  end;

  TTagArray = array of TTag;



  TTagRW = class(TTag)
  protected
    valueWrChanged: Boolean;
  public
    constructor Create(name: string; flags: Integer = 0; writeMutex: TMutex = nil);override;

    function hasWriteValue: boolean;
    function acceptWriteValue: boolean;
    procedure raiseWriteValue;

	  procedure copyLastWriteToRead; virtual; abstract;
	  procedure copyWriteToLastWrite; virtual; abstract;
	  procedure copyWriteToRead; virtual; abstract;

    function getWriteValBool: boolean; virtual; abstract;
    function getWriteValInt: integer; virtual; abstract;
    function getWriteValLong: int64; virtual; abstract;
    function getWriteValDouble: double; virtual; abstract;
    function getWriteValString: string; virtual; abstract;

    procedure setReadValBool(value: boolean); virtual; abstract;
    procedure setReadValInt(value: integer); virtual; abstract;
    procedure setReadValLong(value: int64); virtual; abstract;
    procedure setReadValDouble(value: double); virtual; abstract;
    procedure setReadValString(value: string); virtual; abstract;

  end;

  function createTagRW(ttype: TTagType; name: string; flags: integer = 0; writeMutex: TMutex = nil): TTagRW;


implementation
uses
  TagRWBool, TagRWInt, TagRWLong, TagRWDouble, TagRWString;

{ TTag }


constructor TTag.Create(name: string; flags: Integer; writeMutex: TMutex);
begin
  Self.name := name;
  Self.flags := flags;
  Self.writeMutex := writeMutex;
  status := tsGOOD;
end;

function TTag.getName: string;
begin
  Result := name;
end;


function TTag.getStatus: TTagStatus;
begin
  Result := status;
end;

procedure TTag.setStatus(status: TTagStatus);
begin
  Self.status := status;
end;

function TTag.getFlags: Integer;
begin
  Result := flags;
end;

procedure TTag.setFlags(flags: Integer);
begin
  Self.flags := flags;
end;

function TTag.getTypeName: string;
begin
  case getType of
  ttBOOL:   result := 'BOOL';
  ttINT:    result := 'INT';
  ttLONG:   result := 'LONG';
  ttDOUBLE: result := 'DOUBLE';
  ttSTRING: result := 'STRING';
  end;
end;



{ TTagRW }

constructor TTagRW.Create(name: string; flags: Integer; writeMutex: TMutex);
begin
  inherited Create(name, flags, writeMutex);
  valueWrChanged := false;
end;

function TTagRW.hasWriteValue: boolean;
begin
  if valueWrChanged  then begin
			if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
        if not valueWrChanged then begin
          Result := false;
        end else begin
          copyWriteToLastWrite;
          Result := true;
        end;
        writeMutex.Release;
      end else
  			result := false;
	end	else
		result := false;
end;

function TTagRW.acceptWriteValue: boolean;
begin
  if valueWrChanged  then begin
			if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
        if not valueWrChanged then begin
          Result := false;
        end else begin
          copyWriteToLastWrite;
          copyWriteToRead;
          Result := true;
        end;
        writeMutex.Release;
      end else
  			result := false;
	end else
    result := false;
end;

procedure TTagRW.raiseWriteValue;
begin
	valueWrChanged := True;
end;

function createTagRW(ttype: TTagType; name: string; flags: integer = 0; writeMutex: TMutex = nil): TTagRW;
begin
  Case ttype of
    ttBOOL   : Result := TTagRWBool.Create(name, flags, writeMutex);
    ttINT    : Result := TTagRWInt.Create(name, flags, writeMutex);
    ttLONG   : Result := TTagRWLong.Create(name, flags, writeMutex);
    ttDOUBLE : Result := TTagRWDouble.Create(name, flags, writeMutex);
    ttSTRING : Result := TTagRWString.Create(name, flags, writeMutex);
  end;
end;

end.
