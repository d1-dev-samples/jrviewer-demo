unit JrbustcpClientProp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Mask, JrbustcpClient;

type
  TJrbustcpClientPropForm = class(TForm)
    OkButton: TButton;
    CancelButton: TButton;
    Label1: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label3: TLabel;
    edPort: TEdit;
    edHost: TEdit;
    edPeriod: TEdit;
    edClientDescr: TEdit;
    edRemoteFilter: TEdit;
    pnlTotal: TPanel;
    Label2: TLabel;
    edTimeout: TEdit;
    btTest: TButton;
    cbExcludeExternal: TCheckBox;
    cbIncludeHidden: TCheckBox;
    procedure FormActivate(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
    procedure btTestClick(Sender: TObject);
  private
  public
    clnt: TJrbustcpClient;
  end;

var
  JrbustcpClientPropForm: TJrbustcpClientPropForm;

implementation
uses
  numbers;

{$R *.dfm}

procedure TJrbustcpClientPropForm.FormActivate(Sender: TObject);
begin
  edRemoteFilter.Text := clnt.filter;
  edClientDescr.Text  := clnt.descr;
  edHost.Text         := clnt.host;
  edPort.Text         := IntToStr(clnt.port);
  edPeriod.Text       := IntToStr(clnt.period);
  edTimeout.Text      := IntToStr(clnt.timeout);

  cbExcludeExternal.Checked := (clnt.flags and INITPRM_EXCLUDE_EXTERNAL) > 0;
  cbIncludeHidden.Checked := (clnt.flags and INITPRM_INCLUDE_HIDDEN) > 0;
end;

procedure TJrbustcpClientPropForm.OkButtonClick(Sender: TObject);
begin
  clnt.filter := edRemoteFilter.Text;
  clnt.descr := edClientDescr.Text;
  clnt.host := edHost.Text;
  clnt.port := StrToIntDef(edPort.Text, clnt.port);
  clnt.period := StrToIntDef(edPeriod.Text, clnt.period);
  clnt.timeout := StrToIntDef(edTimeout.Text, clnt.timeout);

  clnt.setFlag(INITPRM_EXCLUDE_EXTERNAL, cbExcludeExternal.Checked);
  clnt.setFlag(INITPRM_INCLUDE_HIDDEN, cbIncludeHidden.Checked);

  ModalResult := mrOk;
end;


procedure TJrbustcpClientPropForm.btTestClick(Sender: TObject);
var
  testclnt: TJrbustcpClient;
begin
  testclnt := TJrbustcpClient.Create;
  testclnt.filter := edRemoteFilter.Text;
  testclnt.descr := edClientDescr.Text;
  testclnt.host := edHost.Text;
  testclnt.port := StrToIntDef(edPort.Text, clnt.port);
  testclnt.period := StrToIntDef(edPeriod.Text, clnt.period);
  testclnt.timeout := StrToIntDef(edTimeout.Text, clnt.timeout);

  testclnt.setFlag(INITPRM_EXCLUDE_EXTERNAL, cbExcludeExternal.Checked);
  testclnt.setFlag(INITPRM_INCLUDE_HIDDEN, cbIncludeHidden.Checked);

  try
    testclnt.Connect;
    if testclnt.Connected then begin
      pnlTotal.Color := clLime;
      pnlTotal.Font.Color := clBlack;
      pnlTotal.Caption := IntToStr(testclnt.TagTable.getSize);
    end else begin
      pnlTotal.Color := clRed;
      pnlTotal.Font.Color := clWhite;
      pnlTotal.Caption := '������';
    end;
    testclnt.Disconnect;
  finally
    testclnt.Free
  end;

end;

end.
