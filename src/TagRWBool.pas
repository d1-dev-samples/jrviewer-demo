unit TagRWBool;

interface
uses
  Tags, Mutex;

type
  TTagRWBool = class(TTagRW)
  private
	  valueRd: Boolean;
	  valueWr: Boolean;
    valueWrLast: Boolean;
  public
    constructor Create(name: string; flags: Integer = 0; writeMutex: TMutex = nil);override;

    function getType: TTagType; override;
    function equalsValue(tag: TTag): Boolean; override;

	  procedure copyLastWriteToRead; override;
	  procedure copyWriteToLastWrite; override;
	  procedure copyWriteToRead; override;


    function getBool: boolean; override;
    function getInt: integer; override;
    function getLong: int64; override;
    function getDouble: double; override;
    function getString: string; override;

    procedure setBool(value: boolean); override;
    procedure setInt(value: integer); override;
    procedure setLong(value: int64); override;
    procedure setDouble(value: double); override;
    procedure setString(value: string); override;


    function getWriteValBool: boolean; override;
    function getWriteValInt: integer; override;
    function getWriteValLong: int64; override;
    function getWriteValDouble: double; override;
    function getWriteValString: string; override;

    procedure setReadValBool(value: boolean); override;
    procedure setReadValInt(value: integer); override;
    procedure setReadValLong(value: int64); override;
    procedure setReadValDouble(value: double); override;
    procedure setReadValString(value: string); override;
  end;


implementation

{ TTagRWBool }

constructor TTagRWBool.Create(name: string; flags: Integer; writeMutex: TMutex);
begin
  inherited Create(name, flags, writeMutex);
  valueRd := false;
  valueWr := false;
  valueWrLast := false;
end;


function TTagRWBool.getType: TTagType;
begin
  Result := ttBOOL;
end;

function TTagRWBool.equalsValue(tag: TTag): Boolean;
begin
  Result := tag.getBool() = valueRd;
end;

procedure TTagRWBool.copyWriteToLastWrite;
begin
		valueWrChanged := false;
		valueWrLast := valueWr;
end;

procedure TTagRWBool.copyLastWriteToRead;
begin
		valueRd := valueWrLast;
end;

procedure TTagRWBool.copyWriteToRead;
begin
		valueRd := valueWr;
end;


// general getters
function TTagRWBool.getBool: boolean;
begin
  Result := valueRd;
end;

function TTagRWBool.getInt: integer;
begin
  if valueRd then Result:=1 else Result:=0;
end;

function TTagRWBool.getLong: int64;
begin
  if valueRd then Result:=1 else Result:=0;
end;

function TTagRWBool.getDouble: double;
begin
  if valueRd then Result:=1.0 else Result:=0.0;
end;

function TTagRWBool.getString: string;
begin
  if valueRd then Result:='on' else Result:='off';
end;


// general setters
procedure TTagRWBool.setBool(value: boolean);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := value;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWBool.setInt(value: integer);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := value <> 0;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWBool.setLong(value: int64);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := value <> 0;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWBool.setDouble(value: double);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := value <> 0.0;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWBool.setString(value: string);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := value = 'on';
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;




// rw getters
function TTagRWBool.getWriteValBool: boolean;
begin
  Result := valueWrLast;
end;

function TTagRWBool.getWriteValInt: integer;
begin
  if valueWrLast then Result:=1 else Result:=0;
end;

function TTagRWBool.getWriteValLong: int64;
begin
  if valueWrLast then Result:=1 else Result:=0;
end;

function TTagRWBool.getWriteValDouble: double;
begin
  if valueWrLast then Result:=1.0 else Result:=0.0;
end;

function TTagRWBool.getWriteValString: string;
begin
  if valueWrLast then Result:='on' else Result:='off';
end;


// rw getters
procedure TTagRWBool.setReadValBool(value: boolean);
begin
  valueRd := value;
end;

procedure TTagRWBool.setReadValInt(value: integer);
begin
  valueRd := value <> 0;
end;

procedure TTagRWBool.setReadValLong(value: int64);
begin
  valueRd := value <> 0;
end;

procedure TTagRWBool.setReadValDouble(value: double);
begin
  valueRd := value <> 0.0;
end;

procedure TTagRWBool.setReadValString(value: string);
begin
		valueRd := value = 'on';
end;



end.
