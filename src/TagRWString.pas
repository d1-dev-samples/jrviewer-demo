unit TagRWString;

interface
uses
  Tags, Mutex;

type
  TTagRWString = class(TTagRW)
  private
	  valueRd: string;
	  valueWr: string;
    valueWrLast: string;
  public
    constructor Create(name: string; flags: Integer = 0; writeMutex: TMutex = nil);override;

    function getType: TTagType; override;
    function equalsValue(tag: TTag): Boolean; override;
    
	  procedure copyLastWriteToRead; override;
	  procedure copyWriteToLastWrite; override;
	  procedure copyWriteToRead; override;


    function getBool: boolean; override;
    function getInt: integer; override;
    function getLong: int64; override;
    function getDouble: double; override;
    function getString: string; override;

    procedure setBool(value: boolean); override;
    procedure setInt(value: integer); override;
    procedure setLong(value: int64); override;
    procedure setDouble(value: double); override;
    procedure setString(value: string); override;


    function getWriteValBool: boolean; override;
    function getWriteValInt: integer; override;
    function getWriteValLong: int64; override;
    function getWriteValDouble: double; override;
    function getWriteValString: string; override;

    procedure setReadValBool(value: boolean); override;
    procedure setReadValInt(value: integer); override;
    procedure setReadValLong(value: int64); override;
    procedure setReadValDouble(value: double); override;
    procedure setReadValString(value: string); override;
  end;


implementation

uses SysUtils;

{ TTagRWString }

constructor TTagRWString.Create(name: string; flags: Integer;
  writeMutex: TMutex);
begin
  inherited Create(name, flags, writeMutex);
  valueRd := '';
  valueWr := '';
  valueWrLast := '';
end;


function TTagRWString.getType: TTagType;
begin
  Result := ttSTRING;
end;

function TTagRWString.equalsValue(tag: TTag): Boolean;
begin
  Result := tag.getString() = valueRd;
end;

procedure TTagRWString.copyWriteToLastWrite;
begin
		valueWrChanged := false;
		valueWrLast := valueWr;
end;

procedure TTagRWString.copyLastWriteToRead;
begin
		valueRd := valueWrLast;
end;

procedure TTagRWString.copyWriteToRead;
begin
		valueRd := valueWr;
end;


// general getters
function TTagRWString.getBool: boolean;
begin
  Result := valueRd = 'on';
end;

function TTagRWString.getInt: integer;
begin
  Result := StrToIntDef(valueRd, 0);
end;

function TTagRWString.getLong: int64;
begin
  Result := StrToInt64Def(valueRd, 0);
end;

function TTagRWString.getDouble: double;
begin
  Result := StrToFloatDef(valueRd, 0.0);
end;

function TTagRWString.getString: string;
begin
  Result := valueRd;
end;


// general setters
procedure TTagRWString.setBool(value: boolean);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		if value then valueWr := 'on' else valueWr := 'off';
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWString.setInt(value: integer);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := inttostr(value);
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWString.setLong(value: int64);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := inttostr(value);
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWString.setDouble(value: double);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := floattostr(value);
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWString.setString(value: string);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := value;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;




// rw getters
function TTagRWString.getWriteValBool: boolean;
begin
  Result := valueWrLast = 'on';
end;

function TTagRWString.getWriteValInt: integer;
begin
  Result := StrToIntDef(valueWrLast, 0);
end;

function TTagRWString.getWriteValLong: int64;
begin
  Result := StrToInt64Def(valueWrLast, 0);
end;

function TTagRWString.getWriteValDouble: double;
begin
  Result := StrToFloatDef(valueWrLast, 0.0);
end;

function TTagRWString.getWriteValString: string;
begin
  Result := valueWrLast;
end;


// rw getters
procedure TTagRWString.setReadValBool(value: boolean);
begin
  if value then valueRd := 'on' else valueRd := 'off';
end;

procedure TTagRWString.setReadValInt(value: integer);
begin
  valueRd := inttostr(value);
end;

procedure TTagRWString.setReadValLong(value: int64);
begin
  valueRd := inttostr(value);
end;

procedure TTagRWString.setReadValDouble(value: double);
begin
  valueRd := floattostr(value);
end;

procedure TTagRWString.setReadValString(value: string);
begin
		valueRd := value;
end;

end.
