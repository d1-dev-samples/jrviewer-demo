unit TagRWDouble;

interface
uses
  SysUtils, Tags, Mutex;

type
  TTagRWDouble = class(TTagRW)
  private
	  valueRd: double;
	  valueWr: double;
    valueWrLast: double;
  public
    constructor Create(name: string; flags: Integer = 0; writeMutex: TMutex = nil);override;

    function getType: TTagType; override;
    function equalsValue(tag: TTag): Boolean; override;
    
	  procedure copyLastWriteToRead; override;
	  procedure copyWriteToLastWrite; override;
	  procedure copyWriteToRead; override;


    function getBool: boolean; override;
    function getInt: integer; override;
    function getLong: int64; override;
    function getDouble: double; override;
    function getString: string; override;

    procedure setBool(value: boolean); override;
    procedure setInt(value: integer); override;
    procedure setLong(value: int64); override;
    procedure setDouble(value: double); override;
    procedure setString(value: string); override;


    function getWriteValBool: boolean; override;
    function getWriteValInt: integer; override;
    function getWriteValLong: int64; override;
    function getWriteValDouble: double; override;
    function getWriteValString: string; override;

    procedure setReadValBool(value: boolean); override;
    procedure setReadValInt(value: integer); override;
    procedure setReadValLong(value: int64); override;
    procedure setReadValDouble(value: double); override;
    procedure setReadValString(value: string); override;
  end;


implementation

{ TTagRWDouble }

constructor TTagRWDouble.Create(name: string; flags: Integer;
  writeMutex: TMutex);
begin
  inherited Create(name, flags, writeMutex);
  valueRd := 0.0;
  valueWr := 0.0;
  valueWrLast := 0.0;
end;


function TTagRWDouble.getType: TTagType;
begin
  Result := ttDOUBLE;
end;

function TTagRWDouble.equalsValue(tag: TTag): Boolean;
begin
  Result := tag.getDouble() = valueRd;
end;

procedure TTagRWDouble.copyWriteToLastWrite;
begin
		valueWrChanged := false;
		valueWrLast := valueWr;
end;

procedure TTagRWDouble.copyLastWriteToRead;
begin
		valueRd := valueWrLast;
end;

procedure TTagRWDouble.copyWriteToRead;
begin
		valueRd := valueWr;
end;


// general getters
function TTagRWDouble.getBool: boolean;
begin
  Result := valueRd <> 0.0;
end;

function TTagRWDouble.getInt: integer;
begin
  Result := Trunc(valueRd);
end;

function TTagRWDouble.getLong: int64;
begin
  Result := Trunc(valueRd);
end;

function TTagRWDouble.getDouble: double;
begin
  Result := valueRd;
end;

function TTagRWDouble.getString: string;
begin
  Result := FloatToStr(valueRd);
end;


// general setters
procedure TTagRWDouble.setBool(value: boolean);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		if value then valueWr := 1.0 else valueWr := 0.0;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWDouble.setInt(value: integer);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := value;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWDouble.setLong(value: int64);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := value;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWDouble.setDouble(value: double);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := value;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWDouble.setString(value: string);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := StrToFloatDef(value, 0.0);
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;




// rw getters
function TTagRWDouble.getWriteValBool: boolean;
begin
  Result := valueWrLast <> 0;
end;

function TTagRWDouble.getWriteValInt: integer;
begin
  Result := Trunc(valueWrLast);
end;

function TTagRWDouble.getWriteValLong: int64;
begin
  Result := Trunc(valueWrLast);
end;

function TTagRWDouble.getWriteValDouble: double;
begin
  Result := valueWrLast;
end;

function TTagRWDouble.getWriteValString: string;
begin
  Result := FloatToStr(valueWrLast);
end;


// rw getters
procedure TTagRWDouble.setReadValBool(value: boolean);
begin
  if value then valueRd := 1 else valueRd := 0;
end;

procedure TTagRWDouble.setReadValInt(value: integer);
begin
  valueRd := value;
end;

procedure TTagRWDouble.setReadValLong(value: int64);
begin
  valueRd := value;
end;

procedure TTagRWDouble.setReadValDouble(value: double);
begin
  valueRd := value;
end;

procedure TTagRWDouble.setReadValString(value: string);
begin
		valueRd := StrToFloatDef(value, 0);   
end;

end.
