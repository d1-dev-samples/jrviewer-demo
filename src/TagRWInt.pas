unit TagRWInt;

interface
uses
  Tags, Mutex;

type
  TTagRWInt = class(TTagRW)
  private
	  valueRd: Integer;
	  valueWr: Integer;
    valueWrLast: Integer;
  public
    constructor Create(name: string; flags: Integer = 0; writeMutex: TMutex = nil);override;

    function getType: TTagType; override;
    function equalsValue(tag: TTag): Boolean; override;
    
	  procedure copyLastWriteToRead; override;
	  procedure copyWriteToLastWrite; override;
	  procedure copyWriteToRead; override;


    function getBool: boolean; override;
    function getInt: integer; override;
    function getLong: int64; override;
    function getDouble: double; override;
    function getString: string; override;

    procedure setBool(value: boolean); override;
    procedure setInt(value: integer); override;
    procedure setLong(value: int64); override;
    procedure setDouble(value: double); override;
    procedure setString(value: string); override;


    function getWriteValBool: boolean; override;
    function getWriteValInt: integer; override;
    function getWriteValLong: int64; override;
    function getWriteValDouble: double; override;
    function getWriteValString: string; override;

    procedure setReadValBool(value: boolean); override;
    procedure setReadValInt(value: integer); override;
    procedure setReadValLong(value: int64); override;
    procedure setReadValDouble(value: double); override;
    procedure setReadValString(value: string); override;
  end;


implementation

uses SysUtils;

{ TTagRWInt }

constructor TTagRWInt.Create(name: string; flags: Integer;
  writeMutex: TMutex);
begin
  inherited Create(name, flags, writeMutex);
  valueRd := 0;
  valueWr := 0;
  valueWrLast := 0;
end;


function TTagRWInt.getType: TTagType;
begin
  Result := ttINT;
end;

function TTagRWInt.equalsValue(tag: TTag): Boolean;
begin
  Result := tag.getInt() = valueRd;
end;

procedure TTagRWInt.copyWriteToLastWrite;
begin
		valueWrChanged := false;
		valueWrLast := valueWr;
end;

procedure TTagRWInt.copyLastWriteToRead;
begin
		valueRd := valueWrLast;
end;

procedure TTagRWInt.copyWriteToRead;
begin
		valueRd := valueWr;
end;


// general getters
function TTagRWInt.getBool: boolean;
begin
  Result := valueRd <> 0;
end;

function TTagRWInt.getInt: integer;
begin
  Result := valueRd;
end;

function TTagRWInt.getLong: int64;
begin
  Result := valueRd;
end;

function TTagRWInt.getDouble: double;
begin
  Result := valueRd;
end;

function TTagRWInt.getString: string;
begin
  Result := IntToStr(valueRd);
end;


// general setters
procedure TTagRWInt.setBool(value: boolean);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		if value then valueWr := 1 else valueWr := 0;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWInt.setInt(value: integer);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := value;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWInt.setLong(value: int64);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := value;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWInt.setDouble(value: double);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := Trunc(value);
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWInt.setString(value: string);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := StrToIntDef(value, 0);
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;




// rw getters
function TTagRWInt.getWriteValBool: boolean;
begin
  Result := valueWrLast <> 0;
end;

function TTagRWInt.getWriteValInt: integer;
begin
  Result := valueWrLast;
end;

function TTagRWInt.getWriteValLong: int64;
begin
  Result := valueWrLast;
end;

function TTagRWInt.getWriteValDouble: double;
begin
  Result := valueWrLast;
end;

function TTagRWInt.getWriteValString: string;
begin
  Result := IntToStr(valueWrLast);
end;


// rw getters
procedure TTagRWInt.setReadValBool(value: boolean);
begin
  if value then valueRd := 1 else valueRd := 0;
end;

procedure TTagRWInt.setReadValInt(value: integer);
begin
  valueRd := value;
end;

procedure TTagRWInt.setReadValLong(value: int64);
begin
  valueRd := value;
end;

procedure TTagRWInt.setReadValDouble(value: double);
begin
  valueRd := Trunc(value);
end;

procedure TTagRWInt.setReadValString(value: string);
begin
		valueRd := StrToIntDef(value, 0);   
end;

end.
