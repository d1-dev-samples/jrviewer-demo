unit JrbustcpClient;

interface
uses Controls, Windows, SysUtils, Classes, Mutex, IdTCPClient, IdGlobal, ByteBuf,
  Tags, TagTable;

const
  INITPRM_TAGDESCR         = 1;
  INITPRM_TAGSTATUS        = 2;
  INITPRM_EXCLUDE_EXTERNAL = 4;
  INITPRM_INCLUDE_HIDDEN   = 8;

  STATE_CREATED = 0;
  STATE_READING = 1;
  STATE_WRITING = 2;
  STATE_SLEEPING = 3;
  STATE_TERMINATED = 4;

type
  TJrbustcpClientThread = class;

  TJrbustcpClient = class(TObject)
  private
    writeMutex: TMutex;
    thread: TJrbustcpClientThread;
    tcpclient: TIdTCPClient;

    FConnected: Boolean;
    FNeedReinitList: Boolean;
    FTagTable: TTagTable;
    FState: Integer;
    FStateDt: TDateTime;

    reqid: Integer;
    lastcmd: Byte;
    outbuf: TByteBuf;
    inbuf: TByteBuf;

    tags: array of TTagRW;

    writeCache: array of Integer;


    destructor Destroy; override;
    procedure OnThreadTerminate(Sender: TObject);

    function InitList: Boolean;
    function Read: Boolean;
    function Write: Boolean;

    procedure startMessage(cmd: integer);
    procedure writeAndReadMessage;

    procedure setState(state: integer);
    function getState: string;
  public
    descr: string;
    filter: string;
    host: string;
    port: Integer;
    flags: Integer;
    period: Integer;
    timeout: Integer;

    Constructor Create;

    function Connect: Boolean;
    function Disconnect: Boolean;

    function showSettingsDialog: Boolean;
    procedure setFlag(flag: Integer; value: boolean);

    property Connected: boolean read FConnected;
    property NeedReinitList: boolean read FNeedReinitList;
    property TagTable: TTagTable read FTagTable;
    property State: string read getState;
  end;

  TJrbustcpClientThread = class(TThread)
  protected
    procedure Execute; override;
  public
    parent: TJrbustcpClient;
  end;

implementation
uses
  JrbustcpClientProp;

const
  MAGIC_HEADER = $ABCD;

  CMD_INIT    = 1;
  CMD_LIST    = 2;
  CMD_UPDATE  = 3;
  CMD_READ    = 4;
  CMD_WRITE   = 5;
  CMD_UNKNOWN = 255;

  VAL_INDEX_SHORT   = $FE;
  VAL_INDEX_MEDIUM  = $FF;


{ TJrbustcpClient }

Constructor TJrbustcpClient.Create;
var
  p: Pointer;
begin
  DecimalSeparator := '.';
  
  writeMutex := TMutex.Create('TJrbustcpClient@' + IntToHex(dword(@self),8));
  inbuf := TByteBuf.Create(16384);
  outbuf := TByteBuf.Create(16384);
  FTagTable := TTagTable.Create;
  setState(STATE_CREATED);

  period := 500;
  host := 'localhost';
  port := 40000;
  timeout := 3000;
  flags := INITPRM_TAGSTATUS;
  SetLength(tags, 0);

  FConnected := False;
  FNeedReinitList := false;
  thread := nil;
  tcpclient := nil;
  Randomize;
end;

destructor TJrbustcpClient.Destroy;
begin
  Disconnect;
  FTagTable.Free;
  inbuf.Free;
  outbuf.Free;
  writeMutex.Free;
end;


function TJrbustcpClient.Connect: Boolean;
begin
  Disconnect;

  tcpclient := TIdTCPClient.Create;
  tcpclient.Host := host;
  tcpclient.Port := port;
  tcpclient.ConnectTimeout := timeout;
  tcpclient.ReadTimeout := timeout;
  try
    tcpclient.Connect;

    if (InitList) and (Read) then begin
      thread := TJrbustcpClientThread.Create(true);
      thread.parent := self;
      thread.Resume;
      thread.OnTerminate := OnThreadTerminate;

      reqid := Random(MaxInt);
      FConnected := true;
    end;
  except
  end;

  if not FConnected then
    Disconnect;

  Result := FConnected;
end;


function TJrbustcpClient.Disconnect: Boolean;
begin
  FConnected := False;

  if thread <> nil then begin
    thread.Terminate;
    thread.WaitFor;
    thread := nil;
  end;

  if tcpclient <> nil then begin
    try
      tcpclient.Disconnect;
    except
    end;
    tcpclient.Free;
    tcpclient := nil;
  end;
end;

procedure TJrbustcpClient.OnThreadTerminate(Sender: TObject);
begin
  FConnected := false;
end;


procedure TJrbustcpClient.startMessage(cmd: integer);
begin
  Inc(reqid);
  outbuf.clear;
  Inc(outbuf.writeIndex, 2);
  outbuf.writeWord(MAGIC_HEADER);
  outbuf.writeInt(reqid);
  outbuf.writeByte(cmd);
  lastcmd := cmd;
end;


procedure TJrbustcpClient.writeAndReadMessage;
var
  size, gotReqid, crc1, crc2: Integer;
  gotCmd: Byte;
  header: Word;
begin
  crc1 := outbuf.calcCrc32(4, outbuf.writeIndex-4);
  outbuf.writeInt(crc1);
  outbuf.setWord(0, outbuf.writeIndex-2);
  tcpclient.IOHandler.Write(outbuf.buf, outbuf.writeIndex);

  repeat
    inbuf.clear;
    size := tcpclient.IOHandler.ReadSmallInt();
    tcpclient.IOHandler.ReadBytes(inbuf.buf, size, false);
    inbuf.writeIndex := size;
    header := inbuf.readWord;
    gotReqid := inbuf.readInt;
    gotCmd := inbuf.readByte;
    crc1 := inbuf.getInt( size-4);
    crc2 := inbuf.calcCrc32(2, size-6);

  until (header=MAGIC_HEADER) and (gotReqid=reqid) and (gotCmd=lastcmd+$80) and (crc1=crc2);
end;


function TJrbustcpClient.InitList: Boolean;
var
  tagname, tagdescr: string;
  size, qnt, next, index, i,n: integer;
  ttype: TTagType;
  b: Byte;
  tag: TTagRW;
  t: Cardinal;
begin
  t := GetTickCount;
  Result := true;
  try
    startMessage(CMD_INIT);
    outbuf.writeSmallSizeString(filter);
    outbuf.writeSmallSizeString(descr);
    outbuf.writeWord(flags);
    writeAndReadMessage;

    for i:=0 to Length(tags)-1 do
      tags[i].setStatus(tsDELETED);

    size := inbuf.readMedium;
    SetLength(tags, size);

    index := 0;
    while index < size do begin
      startMessage(CMD_LIST);
      outbuf.writeMedium(index);
      writeAndReadMessage;

      if index <> inbuf.readMedium then begin
        //todo: log error - invalid index
        result := False;
        break;
      end;
      qnt := inbuf.readMedium;
      inbuf.readMedium; // skip 'next'

      for i:=1 to qnt do begin
        b := inbuf.readByte;
        if (b >= Ord(Low(TTagType))) and (b <= Ord(High(TTagType))) then
          ttype := TTagType(b)
        else begin
          //todo: log error - tagtype not found
          result := False;
          break;
        end;

        tagname := inbuf.readSmallSizeString;
        tagdescr := inbuf.readSmallSizeString;

        tag := TTagRW(TagTable.get(tagname));
        if (tag = nil) or (tag.getType <> ttype) then begin
          tag := createTagRW(ttype, tagname, TAG_FLAG_EXTERNAL, writeMutex);
          tagtable.add(tag);
        end;
        tag.setStatus(tsGOOD);

        tags[index] := tag;
        Inc(index)
      end;

      if not Result then Break;
    end;

    if index <> size then begin
      //todo: log error - didn't get all the tags
      result := false;
    end;
  except
    result := false;
  end;

  if Result then begin
    TagTable.removeDeleted;
    FNeedReinitList := false;
  end else begin
    SetLength(tags, 0);
  end;

  t := GetTickCount - t;
end;

function TJrbustcpClient.Read: Boolean;
var
  qnt, next, index, i, size: integer;
  tag: TTagRW;
  valcode: byte;
begin
  Result := true;
  try
    startMessage(CMD_UPDATE);
    writeAndReadMessage;

    qnt := inbuf.readMedium;
    next := inbuf.readMedium;
    FNeedReinitList := inbuf.readByte = $FF;

    if qnt > 0 then begin
      size := Length(tags);
      repeat
        startMessage(CMD_READ);
        outbuf.writeMedium(next);
        writeAndReadMessage;

        index := inbuf.readMedium;
        qnt := inbuf.readMedium;
        next := inbuf.readMedium;

        for i:=1 to qnt do begin
          valcode := inbuf.getByte( inbuf.readIndex );
          if valcode >= VAL_INDEX_SHORT then begin
            inbuf.readByte;
            if valcode = VAL_INDEX_MEDIUM then
              index := inbuf.readMedium
            else
              index := inbuf.readWord;
          end;

          if index < size then
            inbuf.readValueStatus( tags[index] );

          Inc(index);
        end;
      until next = 0;
    end;
  except
    result := false;
  end;
end;



function TJrbustcpClient.Write: Boolean;
var
  i,n,size,curindex,index,qnt,qntPos: integer;
  gap: boolean;
begin
  Result := true;

  size := Length(tags);
  if Length(writeCache) < size then
    SetLength(writeCache, length(tags));

  qnt := 0;
  for i:=0 to size-1 do
    if tags[i].hasWriteValue then begin
      writeCache[qnt] := i;
      inc(qnt);
    end;

  if qnt > 0 then begin
    i := 0;
    while i < qnt do begin
      curindex := writeCache[i];
      startMessage(CMD_WRITE);
      outbuf.writeMedium(curindex);
      qntPos := outbuf.writeIndex;
      outbuf.writeMedium(0);

      n := 0;
      while i < qnt do begin
        if not outbuf.canWrite then
          Break;

        index := writeCache[i];
        gap := curindex <> index;

        if gap then begin
          if index < $10000 then begin
            outbuf.writeByte(VAL_INDEX_SHORT);
            outbuf.writeWord(index);
          end else begin
            outbuf.writeByte(VAL_INDEX_MEDIUM);
            outbuf.writeMedium(index);
          end;
          curindex := index;
        end;

        outbuf.writeValue(tags[curindex]);
        Inc(i);
        Inc(n);
        Inc(curindex);
      end;

      outbuf.setMedium(qntPos, n);
      writeAndReadMessage;
    end;
  end;

end;




{*************************** TJrbustcpClientThread ****************************}

procedure TJrbustcpClientThread.Execute;
var
  t: Cardinal;
begin
  repeat
    t := GetTickCount;

    try
      parent.setState(STATE_WRITING);
      if not parent.Write then
        Break;

      parent.setState(STATE_READING);
      if not parent.Read then
        Break;
    except
      Break;
    end;

    parent.setState(STATE_SLEEPING);
    while ((GetTickCount - t) < parent.period) and (not Terminated) do
      sleep(1);

  until Terminated;

  parent.setState(STATE_TERMINATED);
end;




function TJrbustcpClient.showSettingsDialog: Boolean;
begin
  with TJrbustcpClientPropForm.Create(nil) do begin
    clnt := Self;
    result := ShowModal = mrOk;
    Free;
  end;
end;

procedure TJrbustcpClient.setFlag(flag: Integer; value: boolean);
begin
  flags := (flags and not flag) + iif(value, flag, 0);
end;

procedure TJrbustcpClient.setState(state: integer);
begin
  FState := state;
  FStateDt := Now;
end;

function TJrbustcpClient.getState: string;
var
  s: string;
begin
  case FState of
    STATE_CREATED: s := 'CREATED';
    STATE_WRITING: s := 'WRITING';
    STATE_READING: s := 'READING';
    STATE_SLEEPING: s := 'SLEEPING';
    STATE_TERMINATED: s := 'TERMINATED';
  else
    s := '?';
  end;
  Result := DateTimeToStr(FStateDt) + ': ' + s;
end;

end.
