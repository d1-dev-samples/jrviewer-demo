unit TagRWLong;

interface
uses
  Tags, Mutex;

type
  TTagRWLong = class(TTagRW)
  private
	  valueRd: Int64;
	  valueWr: Int64;
    valueWrLast: Int64;
  public
    constructor Create(name: string; flags: Integer = 0; writeMutex: TMutex = nil);override;

    function getType: TTagType; override;
    function equalsValue(tag: TTag): Boolean; override;
    
	  procedure copyLastWriteToRead; override;
	  procedure copyWriteToLastWrite; override;
	  procedure copyWriteToRead; override;


    function getBool: boolean; override;
    function getInt: integer; override;
    function getLong: int64; override;
    function getDouble: double; override;
    function getString: string; override;

    procedure setBool(value: boolean); override;
    procedure setInt(value: integer); override;
    procedure setLong(value: int64); override;
    procedure setDouble(value: double); override;
    procedure setString(value: string); override;


    function getWriteValBool: boolean; override;
    function getWriteValInt: integer; override;
    function getWriteValLong: int64; override;
    function getWriteValDouble: double; override;
    function getWriteValString: string; override;

    procedure setReadValBool(value: boolean); override;
    procedure setReadValInt(value: integer); override;
    procedure setReadValLong(value: int64); override;
    procedure setReadValDouble(value: double); override;
    procedure setReadValString(value: string); override;
  end;


implementation

uses SysUtils;

{ TTagRWLong }

constructor TTagRWLong.Create(name: string; flags: Integer;
  writeMutex: TMutex);
begin
  inherited Create(name, flags, writeMutex);
  valueRd := 0;
  valueWr := 0;
  valueWrLast := 0;
end;



function TTagRWLong.getType: TTagType;
begin
  Result := ttLONG;
end;

function TTagRWLong.equalsValue(tag: TTag): Boolean;
begin
  Result := tag.getLong() = valueRd;
end;

procedure TTagRWLong.copyWriteToLastWrite;
begin
		valueWrChanged := false;
		valueWrLast := valueWr;
end;

procedure TTagRWLong.copyLastWriteToRead;
begin
		valueRd := valueWrLast;
end;

procedure TTagRWLong.copyWriteToRead;
begin
		valueRd := valueWr;
end;


// general getters
function TTagRWLong.getBool: boolean;
begin
  Result := valueRd <> 0;
end;

function TTagRWLong.getInt: integer;
begin
  Result := valueRd and $FFFFFFFF;
end;

function TTagRWLong.getLong: int64;
begin
  Result := valueRd;
end;

function TTagRWLong.getDouble: double;
begin
  Result := valueRd;
end;

function TTagRWLong.getString: string;
begin
  Result := IntToStr(valueRd);
end;


// general setters
procedure TTagRWLong.setBool(value: boolean);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		if value then valueWr := 1 else valueWr := 0;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWLong.setInt(value: integer);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := value;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWLong.setLong(value: int64);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := value;
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWLong.setDouble(value: double);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := Trunc(value);
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;

procedure TTagRWLong.setString(value: string);
begin
  if writeMutex.Get(WRITE_MUTEX_TIMEOUT) then begin
		valueWr := StrToInt64Def(value, 0);
		valueWrChanged := true;
    writeMutex.Release;
  end;
end;




// rw getters
function TTagRWLong.getWriteValBool: boolean;
begin
  Result := valueWrLast <> 0;
end;

function TTagRWLong.getWriteValInt: integer;
begin
  Result := valueWrLast and $FFFFFFFF;
end;

function TTagRWLong.getWriteValLong: int64;
begin
  Result := valueWrLast;
end;

function TTagRWLong.getWriteValDouble: double;
begin
  Result := valueWrLast;
end;

function TTagRWLong.getWriteValString: string;
begin
  Result := IntToStr(valueWrLast);
end;


// rw getters
procedure TTagRWLong.setReadValBool(value: boolean);
begin
  if value then valueRd := 1 else valueRd := 0;
end;

procedure TTagRWLong.setReadValInt(value: integer);
begin
  valueRd := value;
end;

procedure TTagRWLong.setReadValLong(value: int64);
begin
  valueRd := value;
end;

procedure TTagRWLong.setReadValDouble(value: double);
begin
  valueRd := Trunc(value);
end;

procedure TTagRWLong.setReadValString(value: string);
begin
		valueRd := StrToInt64Def(value, 0);   
end;

end.
