unit TagTable;

interface
uses Classes, Tags;

type
(*
  1. TagTable destroys tags which have been added into it
  2. Tagnames must be unique
  3. Tags can be replaced in list
*)

  TTagTable = class(TObject)
  private
    tags: TStringList;
  public
    Constructor Create;
    destructor Destroy; override;

    function add(tag: TTag): TTag;

    procedure clear;
    procedure remove(index: integer);
    procedure removeDeleted;

    function getSize: integer;
    function getIndex(name: string): Integer;
    function get(index: integer): TTag; overload;
    function get(name: string): TTag; overload;

  end;

implementation


{ TTagTable }

constructor TTagTable.Create;
begin
  tags := TStringList.Create;
  tags.Sorted := True;
end;

destructor TTagTable.Destroy;
begin
  clear;
  tags.Free;
end;

function TTagTable.add(tag: TTag): TTag;
var
  index: Integer;
begin
  Result := tag;
  if tag = nil then
    Exit;

  index := getIndex(tag.getName);
  if index < 0 then begin
    tags.AddObject(tag.getName, tag);
  end else begin
    TTag(tags.Objects[index]).Free;
    tags.Objects[index] := tag;
  end;
end;

function TTagTable.getIndex(name: string): Integer;
var
  index: Integer;
begin
  if tags.Find(name, index) then
    Result := index
  else
    Result := -1;
end;

function TTagTable.get(name: string): TTag;
var
  index: Integer;
begin
  if tags.Find(name, index) then
    Result := TTag(tags.Objects[index])
  else
    Result := nil;
end;

function TTagTable.get(index: integer): TTag;
begin
  Result := TTag(Tags.Objects[index]);
end;

procedure TTagTable.remove(index: integer);
begin
  if index < getSize then begin
    get(index).Free;
    tags.Delete(index);
  end;
end;

procedure TTagTable.removeDeleted;
var
  i: integer;
begin
    i:=0;
    while i < getSize do begin
      if get(i).getStatus = tsDELETED then begin
        get(i).Free;
        tags.Delete(i);
      end else
        Inc(i);
    end;
end;

function TTagTable.getSize: integer;
begin
  Result := tags.Count;
end;


procedure TTagTable.clear;
var
  i: Integer;
begin
  for i:=0 to tags.Count-1 do
    TTag(tags.Objects[i]).Free;
  Tags.Clear;
end;

end.
