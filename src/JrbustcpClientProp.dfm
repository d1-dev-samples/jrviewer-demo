object JrbustcpClientPropForm: TJrbustcpClientPropForm
  Left = 204
  Top = 118
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' JrBusTcp '#1082#1083#1080#1077#1085#1090#1072
  ClientHeight = 283
  ClientWidth = 563
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  DesignSize = (
    563
    283)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 54
    Width = 109
    Height = 14
    Caption = #1060#1080#1083#1100#1090#1088' '#1090#1077#1075#1086#1074' (regex)'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4605510
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 16
    Top = 130
    Width = 67
    Height = 14
    Caption = #1048#1084#1103' '#1082#1083#1080#1077#1085#1090#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4605510
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 16
    Top = 6
    Width = 38
    Height = 14
    Caption = #1057#1077#1088#1074#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4605510
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 172
    Top = 6
    Width = 25
    Height = 14
    Caption = #1055#1086#1088#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4605510
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 16
    Top = 178
    Width = 126
    Height = 14
    Caption = #1055#1077#1088#1080#1086#1076' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103' (ms)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4605510
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 228
    Width = 47
    Height = 14
    Anchors = [akLeft, akBottom]
    Caption = 'Total tags'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4605510
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 455
    Top = 6
    Width = 73
    Height = 14
    Anchors = [akTop, akRight]
    Caption = #1058#1072#1081#1084'-'#1072#1091#1090' ('#1084#1089')'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4605510
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object OkButton: TButton
    Left = 359
    Top = 241
    Width = 93
    Height = 30
    Anchors = [akRight, akBottom]
    Caption = #1054#1082
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    OnClick = OkButtonClick
  end
  object CancelButton: TButton
    Left = 456
    Top = 241
    Width = 93
    Height = 30
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Arial'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 9
  end
  object edPort: TEdit
    Left = 172
    Top = 20
    Width = 57
    Height = 28
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object edHost: TEdit
    Left = 16
    Top = 20
    Width = 141
    Height = 28
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object edPeriod: TEdit
    Left = 16
    Top = 192
    Width = 89
    Height = 28
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
  end
  object edClientDescr: TEdit
    Left = 16
    Top = 144
    Width = 528
    Height = 28
    TabStop = False
    Anchors = [akLeft, akTop, akRight]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
  object edRemoteFilter: TEdit
    Left = 16
    Top = 68
    Width = 528
    Height = 28
    TabStop = False
    Anchors = [akLeft, akTop, akRight]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  object pnlTotal: TPanel
    Left = 16
    Top = 242
    Width = 89
    Height = 28
    Anchors = [akLeft, akBottom]
    BevelOuter = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
  end
  object edTimeout: TEdit
    Left = 455
    Top = 20
    Width = 89
    Height = 28
    TabStop = False
    Anchors = [akTop, akRight]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object btTest: TButton
    Left = 116
    Top = 242
    Width = 93
    Height = 30
    Anchors = [akLeft, akBottom]
    Caption = #1058#1077#1089#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnClick = btTestClick
  end
  object cbExcludeExternal: TCheckBox
    Left = 16
    Top = 104
    Width = 260
    Height = 17
    Caption = #1053#1077' '#1087#1088#1080#1085#1080#1084#1072#1090#1100' '#1074#1085#1077#1096#1085#1080#1077' '#1090#1077#1075#1080' (external)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
  end
  object cbIncludeHidden: TCheckBox
    Left = 320
    Top = 104
    Width = 225
    Height = 17
    Caption = #1055#1088#1080#1085#1080#1084#1072#1090#1100' '#1089#1082#1088#1099#1090#1099#1077' '#1090#1077#1075#1080' (hidden)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
  end
end
